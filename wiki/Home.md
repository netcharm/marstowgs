# Welcome

Welcome to your wiki! This is the default page we've installed for your convenience. Go ahead and edit it.

## Wiki features

```
$ hg clone https://bitbucket.org/netcharm/marstowgs/wiki
```

Mars To WGS84
=============
Using "Mars2Wgs.txt" offset data source to adjust china map position.
Thanks for "PC老头子", "米老头", core code take from yours sites.

Features
========
1. Support convert china map position to WGS84(GPS) position.
2. Support convert WGS84(GPS) position to china map position.
3. Support convert GPX/KML file point position from Mars to WGS84
4. Support two method of position offset add/remove: looktable, formula.
5. Support Drag-Drop a file to source file box and auto change target file name.

Todo
====
1. Support KMZ file converting

#SnapShots
![UI MainForm](snapshots/Mars2WGS.png "Mars2WGS")

Have fun!
